---
title: "Photos"
date: 2019-07-14T16:19:07+02:00
draft: false
---

<center>Click any picture below to enlarge it.
<br><b>Note that several of the original images are several megabytes large. It is recommended to click on the images while using a WiFi connection.</b></center>

# Animal Life
{{< gallery dir="/img/entries/AL/" caption-effect="none" />}}

# Bird Life
{{< gallery dir="/img/entries/BL/" caption-effect="none" />}}

# Endangered Species
{{< gallery dir="/img/entries/ES/" caption-effect="none" />}}

# Landscape
{{< gallery dir="/img/entries/L/" caption-effect="none" />}}

# Pollution
{{< gallery dir="/img/entries/P/" caption-effect="none" />}}

# Plant Life
{{< gallery dir="/img/entries/PL/" caption-effect="none" />}}

# Weather Phenomenon
{{< gallery dir="/img/entries/WP/" caption-effect="none" />}}


<center><h1>Voting has now closed for the 2022/23 environmental photo competition</h1></center>
